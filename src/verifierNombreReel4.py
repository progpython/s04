# -*- coding: UTF-8 -*-
'''
Created on 2015-02-01
Python: 3.4
@author: Johnny Tsheke
Comme  float accepte aussi les nombres entiers, une manière
de vérifier si on a un nombre réel strict est de générer
une erreur si l'utilisateur entre un nombre entier
'''
print("Vérification nombre réel en exclusion des entiers")
nombre=input("Entrez un nombre svp\n")

try:
    try:
        nombre=int(nombre)
        raise TypeError("Entier")
        # on genere une erreur de type "TypeError" si un entier
    except ValueError:
        #ici on n'attrape que les erreurs de type "ValueError"
        #les erreurs "TypeError" sont pas attrapé ici mais au niveau supérieur
        nombre=float(nombre)
        print(nombre," est un nombre réel strict")   
except:
    print(nombre, " n'est pas un nombre réel strict")




    
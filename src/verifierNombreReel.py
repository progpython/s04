# -*- coding: UTF-8 -*-
'''
Created on 2014-09-29
Python: 3.4
@author: Johnny Tsheke
'''

print("Vérification nombre réel en exclusion des entiers")
nombre=input("Entrer un nombre svp\n")
nombreSfx=nombre
if((nombre[0]=='-') or (nombre[0]=='+')):
    nombreSfx=nombre[1:] #on copie le suffixe à partir du 2e caractère

if('.' in nombreSfx):
    nombreSfx=nombreSfx.replace('.','',1)# on enleve un seul .
    if(nombreSfx.isdigit()):#on verifie si ca ne reste que des chiffres
        nombre=eval(nombre)
        print(nombre," est un nombre réel")
    else:
        print(nombre, " n'est pas un nombre réel")   
else:
    print(nombre, " n'est pas un nombre réel")
    


    
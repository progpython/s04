# -*- coding: UTF-8 -*-
'''
Update: 2022-04-30
Python: 3.10
@author: Johnny Tsheke
'''


print("Détermination du stade d'âge")
print("En utilisant l'instruction match")
age=input("Entrez le nombre d'année svp:\n")
age=eval(age)

match age:
    case age if (age<12):
       print("Enfant")
    case age if (age >= 12 and age<18):
       print("Adolescent")
    case age if (age >=18 and age<60):
       print("Adulte")
    case _:
       print("Vieux")
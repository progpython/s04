# -*- coding: UTF-8 -*-
'''
Created on 2022-04-30
Python: 3.10
@author: Johnny Tsheke
'''

genre = input("Entrer le genre svp: M ou F\n")

match genre:
   case 'M':
       print("Genre Masculin: ", genre)
   case 'F':
       print("Genre Féminin: ", genre)
   case _:
       print("Autre Genre: ", genre)
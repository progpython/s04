# -*- coding: UTF-8 -*-
'''
Created on 2014-09-29
Python: 3.4
@author: Johnny Tsheke
'''

print("Vérification nombre entier")
nombre=input("Entrer un nombre svp\n")
nombreSfx=nombre
if((nombre[0]=='-') or (nombre[0]=='+')):
    nombreSfx=nombre[1:] #on copie le suffixe à partir du 2e caractère

if(nombreSfx.isdigit()):
    nombre=eval(nombre)
    print(nombre," est un nombre entier")
else:
    print(nombre, "n'est pas un nombre entier")

    
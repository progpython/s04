# -*- coding: UTF-8 -*-
'''
Created on 2015-02-01
Python: 3.4
@author: Johnny Tsheke
'''

print("Vérification nombre réel en exclusion des entiers")
nombre=input("Entrez un nombre svp\n")

try:
    nombre=int(nombre)
    print(nombre, " est nombre entier et pas un nombre réel strict")  
except:
    try:
        nombre=float(nombre)
        print(nombre," est un nombre réel strict")
    except:
        print(nombre, " n'est pas un nombre réel strict")




    
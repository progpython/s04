/**
 * @author Johnny Tsheke @ UQAM
 * 2017-01-29
 */
package test1;

import java.util.*;
public class Conversion {
    private final static double NEUF = 9.0;
    private final static double CINQ = 5.0;
    private final static double TRENTE_DEUX = 32.0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Scanner clavier = new Scanner(System.in);
      System.out.println("Entrez la température en dégrés Celsius svp");
      double tempCelsius = clavier.nextDouble();
      double tempFahrenheit = tempCelsius * (NEUF/CINQ) + TRENTE_DEUX;
      System.out.println(tempCelsius+" dégrés Celsius égal à "+
    		  tempFahrenheit+" dégrés Fahrenheit");
      clavier.close();
	}

}

# -*- coding: UTF-8 -*-
'''
update: 2022-04-30
Python: 3.10
@author: Johnny Tsheke -- UQAM
conversion de dégrés Celsius en dégrés Fahrenheit
'''
def convertir(): #declaration d'une fonction
    x = input("Entrez la température en dégrés Celsius svp\n")
    x=eval(x)
    y = x * (9.0/5.0) + 32
    print(x," dégrés Celsius égal à ",y," dégrés Fahrenheit\n")

programmeur={"nom":"Gagnon","prenom":"Solange","nas":"999999999"}
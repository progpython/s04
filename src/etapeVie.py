# -*- coding: UTF-8 -*-
'''
Update: 2022-04-30
Python: 3.10
@author: Johnny Tsheke
'''


print("Détermination du stade d'âge")
age=input("Entrez le nombre d'année svp\n")
age=eval(age)
if (age<12):
    print("Enfant")
elif (age >= 12 and age<18):
    print("Adolescent")
elif (age >=18 and age<60):
    print("Adulte")
else:
    print("Vieux")